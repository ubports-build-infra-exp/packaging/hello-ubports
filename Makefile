INSTALL := install
DESTDIR ?=
prefix ?= /usr/local

hello-ubports:

.PHONY: install

install: hello-ubports
	$(INSTALL) -m755 -D hello-ubports $(DESTDIR)$(prefix)/bin/hello-ubports
